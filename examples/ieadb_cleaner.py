import re, os, glob
import numpy as np
import pandas as pd
from datetime import datetime

scrapeddir = os.path.dirname(__file__) + '/scraped/'
cleandir = os.path.dirname(__file__) + '/cleaned/'
scrapedfns = glob.glob(scrapeddir + '*txt')
scraped = pd.DataFrame([s.split('/')[-1] for s in scrapedfns], columns=['fn'])
scraped['text'] = [open(f, 'r', encoding='utf-8').read() for f in scrapedfns]
scraped0 = scraped.copy()
scraped.text = scraped.text.str.replace('\ufeff','', regex=False).str.replace(u'\xa0+\s+',u'\t', regex=True).str.strip()

# reconstruct MARPOL 1973
marpoldf = scraped[scraped.fn.str.contains('1973-PollutionFromShips')].copy().sort_values(by='fn')
marpoldf = marpoldf.iloc[[0]].append(marpoldf[marpoldf.fn.str.contains('Protocol')]).append(marpoldf[marpoldf.fn.str.contains('Annex')])
sources = marpoldf.text.str.split('-----(?=\n\[Source)').str[1]
marpoldf.text = marpoldf.text.str.split('(?=-----\n\[Source)').str[0]
marpoldf.text.iloc[0] = marpoldf.text.str.cat() + '-----' + sources.str.cat()
scraped = scraped[~scraped.fn.str.contains('1973-PollutionFromShips')].append(marpoldf.iloc[[0]], ignore_index=True)
scraped0 = scraped0[scraped0.fn.isin(scraped.fn)].reset_index(drop=True)

# add 'last modified' date
today = str(datetime.utcnow().isoformat().split("T")[0])
scraped.text[scraped.text != scraped0.text] = scraped.text[scraped.text != scraped0.text].str.replace('(?<=last retrieved \d{4}-\d{2}-\d{2})', ', last modified ' + today, regex=True)

# iterate over texts and update cleaned directory when nec.
for i in range(len(scraped)):
  # if no modification was made by the cleaner
  if (scraped.text[i] == scraped0.text[i]):
    # copy scraped file to cleaned directory if nec. (update only)
    os.system('cp -pu ' + scrapeddir + scraped.fn[i] + ' ' + cleandir + scraped.fn[i])
  # otherwise, if a modification was made and there is a version in the cleaned directory  
  elif os.path.exists(cleandir + scraped.fn[i]):
    # open the existing cleaned text file and read it into a string (without source & date info)  
    with open(cleandir + scraped.fn[i], "r", encoding='utf-8') as f:
      oldt = f.read().split('\n\n-----\n[Source')[0]
    # remove source & date info from new text  
    newt = scraped.text[i].split('\n\n-----\n[Source')[0]
    # save the new text only if it differs from the previous version
    if (newt != oldt):
       with open(cleandir + scraped.fn[i], 'w', encoding='utf-8') as f:
         f.write(scraped.text[i])
  # if edits were made and there is no existing version in the cleaned directory, save the cleaned text       
  else:            
    with open(cleandir + scraped.fn[i], 'w', encoding='utf-8') as f:
       f.write(scraped.text[i])

