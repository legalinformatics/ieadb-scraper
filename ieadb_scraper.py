# -*- coding: utf-8 -*-

import requests, re, os, time
from lxml.html import fromstring, tostring
from datetime import datetime

# path to file containing URLs of pages to scrape (one URL per line)
urlfile = 'examples/ieadb_urls.txt'
# path to target directory for saving scraped texts
targetdir = 'examples/scraped/'
# user agent specification is necessary for successful requests
headers = {'User-Agent': 'lexbot'}

with open(urlfile, "rt") as f:
    urls = [url.strip() for url in f.readlines()]

for url in urls:
    res = requests.get(url, headers=headers)
    doc = fromstring(res.content)
    # find text
    textel = doc.xpath('//div[@id="block-system-main"]')[0]
    # add missing newlines
    for el in textel.xpath('//p'):
        if (el.tail is not None):
            el.tail += '\n'
    # concatenate text and strip of whitespace at beginning and end
    textstr = tostring(textel, method='text', encoding='unicode').strip()
    # remove source information (is already in CSV file)
    textstr = re.sub('\n(?:Source|SRC): ?(?:\w.+|(?=\n))', '', textstr)
    # add page URL and datetime of retrieval
    fullstr = (textstr +'\n\n-----\n[Source: '+ url +' (last retrieved '+
               str(datetime.utcnow().isoformat().split("T")[0]) +')]')
    # exctract original filename
    filename = doc.xpath('//span[preceding-sibling::strong[text()="Filename: "]]/text()')[0]
    # save text if new or different from existing file
    if os.path.exists(targetdir + filename):
        with open(targetdir + filename, "r", encoding='utf-8') as f:
            oldtext = f.read().split('\n\n-----\n[Source')[0].strip()
        if (textstr != oldtext):
            print(filename, 'new text length: ' + str(len(textstr)) + ', old text length: ' + str(len(oldtext)))
            with open(targetdir + filename, 'w', encoding='utf-8') as f:
                f.write(fullstr)          
    else:            
        with open(targetdir + filename, 'w', encoding='utf-8') as f:
            f.write(fullstr)
    # wait 10 seconds before next request (as required by https://iea.uoregon.edu/robots.txt)
    time.sleep(10)

print(f'Done fetching {len(urls)} texts.')
