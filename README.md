# IEAdb scraper

This repository provides a little tool for fetching treaty texts from the International Environmental Agreements database ([IEAdb](https://iea.uoregon.edu/)) given a set of URLs, and a cleaning/reconstruction script that tries to get as close to the original agreement text as possible.

The components are:
- `ieadb_scraper.py`: the data collection script
- 'examples' directory:
   - `ieadb_urls.txt`: sample agreement URLs
   - `ieadb_cleaner.py`: cleaning/reconstruction script for sample agreements
   - 'scraped': folder containing output of `ieadb_scraper.py` tool
   - 'cleaned': folder containing output of `ieadb_cleaner.py` tool

Following best practices in reproducible research, the data collection and cleaning process is entirely scripted/automated, and both the 'raw' and cleaned data is shared, together with the scripts. The computational environment is also reproducible thanks to [Pipenv](https://pipenv.pypa.io/) (see below).

## About the data
The IEAdb project is probably the largest academic database of international environmental agreements (IEAs) and aims to be a one-stop-shop for IEAs, including texts, metadata and performance indicators.

Citations required for use of the data:

>>>
Data from Ronald B. Mitchell. 2002-2023. International Environmental Agreements Database Project (Version 2020.1).
Available at: http://iea.uoregon.edu/ Date accessed: 01 February 2023

Data described in: Ronald B. Mitchell, Liliana B. Andonova, Mark Axelrod, Jörg Balsiger, Thomas Bernauer, Jessica F. Green, James Hollway, Rakhyun E. Kim and Jean-Frédéric Morin. 2020. What We Know (and Could Know) About International Environmental Agreements. Global Environmental Politics 20:1 (February), 103-121. https://doi.org/10.1162/glep_a_00544
>>>

Its creator and collaborators are political scientists as far as I know, and for international relations research it may be of sufficiently high quality, but for legal research I would caution against using it as the main or only data source. I explored the full database in some depth during my PhD research because I was going to use it as my main data source, but eventually decided to keep it only as a stopgap. For treaty texts, the Australasian Legal Information Institute (AustLII) turned out to be a better source, and for metadata, the United Nations Treaty Series (UNTS) is the best source. The agreements in the 'examples' folder are the only ones from the IEAdb I retained for my text analysis. They would not be suitable for text processing which requires high accuracy or consistency in whitespace distribution (see e.g. missing spaces after paragraph numbers in the file named 1960-ProtectionWorkersAgainstIonizingRadiations.EN.txt).

The `ieadb_scraper.py` bot should work for the full IEAdb dataset (I fetched all English language MEAs at some point), but the `ieadb_cleaner.py` tool is geared towards the small subset of agreements listed in `ieadb_urls.txt`. Cleaning and reconstructing other IEAdb treaty texts would likely require significant additional effort, and I'd recommend choosing the [AustLII-scraper](https://gitlab.com/legalinformatics/austlii-scraper) with its cleaner instead. As everyone's needs and requirements are different, I am still sharing this tool here in case it can be useful to others.

More on treaty data science sources and tools at https://martinakunz.gitlab.io/treaty-analytics/

## Software dependencies
- Operating system: I use Debian Linux and have not tested the code on other systems, but it should be cross-platform (Windows users would need to modify file paths in the script from `/` to `\` I presume)
- Python: I use Python 3.9 but other versions >3.5 may work as well (see `requirements.txt`)
- Python packages: See the `requirements.txt` file or `Pipfile.lock` for the exact versions I used in the last run, and `Pipfile` for minimal dependency specifications -- I recommend using [Pipenv](https://pipenv.pypa.io/) for Python dependency management when reproducible environments matter
  - `pipenv install --ignore-pipfile` installs packages based on `Pipfile.lock` which enables a deterministic build
  - `pipenv check` conducts a safety assessment and lists known vulnerabilities of the packages installed (none at the time of the last run)

To reproduce the computational environment on a new Debian or Ubuntu computer you could run:

```
sudo apt update && sudo apt install git python3.9 python3-pip
pip3 install --user pipenv==2023.10.3
git clone https://gitlab.com/legalinformatics/ieadb-scraper.git
cd ieadb-scraper
pipenv install --ignore-pipfile 
```

## How it works

1. (Optionally) adjust `ieadb_urls.txt` manually or programmatically, adding and/or removing URLs (see below)
2. (Optionally) change target directory in `ieadb_scraper.py` (Emacs is a great text and code editor if you are new to this)
3. In a terminal, `cd` into this folder ('ieadb-scraper') and run the scraper with `python3 ieadb_scraper.py`
4. Check the target directory ('scraped') to see whether the files were downloaded
5. Run the cleaner with `python3 examples/ieadb_cleaner.py`

***Note:*** If you use this repository without changes and there is no difference between the remote and local IEAdb texts, then the local files (in the 'examples' folder) will not be modified. If there is even the slightest difference between the two texts (a single character difference is enough), the scraper will print out the filename and the new and old text lengths (number of characters). Any difference should be double-checked to ensure the change is for the better (e.g. data providers fixing OCR errors), as treaty texts are not supposed to change. Similarly, the cleaning script checks existing files and only adds a 'last modified' datetime to those that it does modify.

## Responsible use
Please DO NOT unnecessarily overload the IEAdb servers! Only run the script if and when you really need it. If you are going to download a large number of texts, or repeatedly fetch them, it might be best to switch to something like [Scrapy](https://scrapy.org/) which can automatically follow best practices in web crawling like identifying the user agent, complying with robots.txt, caching responses, throttling requests etc. The [AustLII-scraper](https://gitlab.com/legalinformatics/austlii-scraper) may serve as an example. 

## Customization

IEADB thankfully provides [CSV files](https://iea.uoregon.edu/current_ieadb_dataset) for exporting IEA metadata, and the treaty text URLs needed for this scraper can easily be constructed from them. Not all entries have texts, however. For instance, [db_treaties.csv](https://iea.uoregon.edu/file/dbtreatiescsv) from 2022-12-26 has 5814 rows in total, 2140 of which have texts. Only rows with a non-empty cell in the 'Treaty Text' column have a text page. The `auto-matched-treaties.csv` file in the [Treaty-identifiers](https://gitlab.com/legalinformatics/treaty-identifiers) repository takes this into account and contains text URLs accordingly. If you wanted to, say, save all IEADB text URLs in `auto-matched-treaties.csv` to a file for scraping with the IEADB-scraper, this would be as simple as downloading the CSV file to a convenient location and processing it as follows (adapting the file paths as necessary):

```python
import numpy as np
import pandas as pd

# reading data from CSV file
data = pd.read_csv('auto-matched-treaties.csv', encoding='utf-8')
# extracting available IEADB text URLs
urls = data.IEADBtextURL[data.IEADBtextURL.notna()]
# saving URLs to file (one per line, without index or header)
urls.to_csv('ieadb_urls.txt', encoding='utf-8', index=False, header=False)
```
